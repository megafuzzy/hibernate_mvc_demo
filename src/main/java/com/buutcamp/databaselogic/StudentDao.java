package com.buutcamp.databaselogic;

import com.buutcamp.objects.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public class StudentDao {


    //make spring recognize this class as a repository
    //get a sessionfactory reference to access data
    @Autowired //todo: create bean for sessionfactory
    private SessionFactory sessionFactory;
@Transactional
    public void saveStudent(Student student) {
        Session session = sessionFactory.getCurrentSession();
        session.save(student);

    }

    //get all data in the database
    @Transactional
    public List<Student> getStudents() {

        Session session = sessionFactory.getCurrentSession();
        //session.beginTransaction();
        Query<Student> query = session.createQuery("from Student", Student.class); //HQL!
        //session.getTransaction().commit();
        //session.close();
        List<Student> mList = query.getResultList();

        return mList;
    }


    @Transactional
    public void deleteStudent(int id) {
        Session session = sessionFactory.getCurrentSession();
        Query query = session.createQuery("delete from Student where id=", Student.class);

        query.setParameter("studentId", id);
        query.executeUpdate(); //TODO


    }
}
