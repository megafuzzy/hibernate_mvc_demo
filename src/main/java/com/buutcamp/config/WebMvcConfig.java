package com.buutcamp.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;
import org.hibernate.SessionFactory;
import org.mariadb.jdbc.MariaDbDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;
import java.util.Random;

@Configuration
@ComponentScan("com.buutcamp")
@EnableWebMvc
@EnableTransactionManagement
public class WebMvcConfig {

    @Bean
    public UrlBasedViewResolver urlBasedViewResolver() {
        UrlBasedViewResolver resolver = new UrlBasedViewResolver() ;
        resolver.setPrefix("/WEB-INF/views/");
        resolver.setSuffix(".jsp");
        //resolver.setPrefix(prefix);
        //resolver.setSuffix(suffix);
        resolver.setViewClass(JstlView.class);
        return resolver;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() throws SQLException {

        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(myDataSource());
        sessionFactoryBean.setPackagesToScan("com.buutcamp.objects");
        sessionFactoryBean.setHibernateProperties(hibernateProperties());

        return sessionFactoryBean;
    }

    @Bean
    @Autowired
    public HibernateTransactionManager transactionManager (SessionFactory sessionFactory) {
                HibernateTransactionManager txManager = new HibernateTransactionManager();
                txManager.setSessionFactory(sessionFactory);
                return txManager;


    }

    DataSource myDataSource() throws SQLException {
        MariaDbDataSource dataSource = new MariaDbDataSource();
            dataSource.setDatabaseName("dev_db");
            dataSource.setUserName("devuser");
            dataSource.setPassword("devuser");
            dataSource.setPort(3307);
            return dataSource;
    }

    Properties hibernateProperties() {
        return new Properties() {
            {
            setProperty("hibernate.hbm2ddl.auto","update");

            setProperty("hibernate.show_sql","true");

            setProperty("hibernate.dialect","org.hibernate.dialect.MariaDBDialect");
            }
        };
    }
}
