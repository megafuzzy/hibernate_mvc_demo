package com.buutcamp.main;

import com.buutcamp.databaselogic.StudentDao;
import com.buutcamp.objects.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
public class AppController {



    @Autowired
    private StudentDao studentDao;

    @GetMapping("/")
    public String frontPageGET(Model model) {

        //model.addAttribute("webBeans", "Session user age: "+sessionUser.getAge()+ " Request user age: "+requestUser.getAge());
        List<Student> students = studentDao.getStudents();
        for (Student s : students) {
            s.toString();
        }
        model.addAttribute("students", students);
        model.addAttribute("student", new Student());

        return "front-page";
    }

    @PostMapping("/saveStudent")
    public String saveStudent(@ModelAttribute("student") Student student, Model model) {
        studentDao.saveStudent(student);
        return "redirect:/";
    }

    @GetMapping("/deleteStudent")
    public String deleteStudent(@RequestParam("studentId") int id) {
        studentDao.deleteStudent(id);
        return "redirect:/";
    }

}
