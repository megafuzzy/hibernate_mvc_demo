<%@ taglib prefix = "c" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>
<head>
<!-- <link rel="stylesheet" href="myStyle.css"> -->
</head>
<header><title>This is first page</title></header>
<body>
Hello hibernate<br/>

<h1> My awesome list </h1>
<table>
<tr>
<th>First name</th>
<th>Last name</th>
<th>Age</th>
</tr>

<c:forEach var="student" items="${students}">
<c:url var="deleteLink" value="deleteStudent">
<c:param name="studentId" value="${student.id}"/>
</c:url>
<tr>
<td>${student.firstName}</td>
<td>${student.lastName}</td>
<td><a href="${deleteLink}" onclick="if (!(confirm("Are you sure you want to delete?"))) return false>Delete></td>
</tr>
</c:forEach>

</table>

<h3>Add new student</h3>

 <form:form method="POST" action="saveStudent" modelAttribute="student">

 <table>
 <tr>
       <td>First name:</td>
       <td><form:input path="firstName"/></td>

   </tr>
  <tr>
        <td>Last name:</td>
        <td><form:input path="lastName"/></td>

   </tr>
        <tr>
             <td><input type="submit" value="save"/></td>
        </tr>
 </table>
 </form:form>

 <!-- <c:out value="${webBeans}"/> -->

</body>
</html>